const express = require('express')
const router = express.Router()
const Building = require('../models/Building')

const getBuildings = async function (req, res, next) {
  const buildings = await Building.find({})
  res.status(200).json(buildings)
}

const addBuilding = async function (req, res, next) {
  try {
    const building = new Building({
      name: req.body.name,
      level: req.body.level
    })
    await building.save()
    res.status(201).json(building)
  } catch (e) {
    res.status(409).json({ message: e.message })
  }
}

const getBuilding = async function (req, res, next) {
  const building = await Building.findById(req.params.id)
  res.status(200).json(building)
}

const updateBuilding = async function (req, res, next) {
  const building = await Building.findByIdAndUpdate(req.params.id, req.body, { new: true })
  res.status(200).json(building)
}

/* GET users listing. */
router.get('/', getBuildings)
router.post('/', addBuilding)
router.get('/:id', getBuilding)
router.put('/:id', updateBuilding)

module.exports = router
